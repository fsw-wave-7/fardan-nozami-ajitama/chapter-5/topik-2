const fs = require("fs");
const file = fs.readFileSync("./data/post.json", "utf-8");
const post = JSON.parse(file);
const { successResponse } = require("../helpers/response");

class Post {
  constructor() {
    this.post = post;
  }

  // Menulis di file post.json
  writeToFile = () => {
    fs.writeFileSync("./data/post.json", JSON.stringify(this.post));
  };

  getPost = (req, res) => {
    successResponse(res, 200, this.post, {
      total: this.post.length,
    });
  };

  getDetailPost = (req, res) => {
    const index = req.params.index;
    successResponse(res, 200, this.post[index]);
    // res.status(200).json({
    //   data: this.post[index],
    //   meta: {
    //     code: 200,
    //     message: "sukses mengambil data",
    //   },
    // });
  };

  insertPost = (req, res) => {
    const body = req.body;

    const param = {
      text: body.text,
      created_at: new Date(),
      username: body.username,
    };

    this.post.push(param);
    successResponse(res, 201, param);
    this.writeToFile();
    // res.status(201).json({
    //   data: param,
    //   meta: {
    //     code: 201,
    //     message: "sukses menyimpan data",
    //   },
    // });
  };

  updatePost = (req, res) => {
    const index = req.params.index;
    const body = req.body;

    this.post[index].text = body.text;
    this.post[index].username = body.username;
    successResponse(res, 200, this.post[index]);
    this.writeToFile();
    // res.status(200).json({
    //   data: this.post[index],
    //   meta: {
    //     code: 200,
    //     message: "sukses mengubah data",
    //   },
    // });
  };

  deletePost = (req, res) => {
    const index = req.params.index;

    this.post.splice(index, 1);
    successResponse(res, 200, null);
    this.writeToFile();
    // res.status(200).json({
    //   data: null,
    //   meta: {
    //     code: 200,
    //     message: "sukses menghapus data",
    //   },
    // });
  };
}

module.exports = Post;
