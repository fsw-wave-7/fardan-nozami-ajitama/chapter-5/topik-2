const fs = require("fs");
const file = fs.readFileSync("./data/users.json", "utf-8");
const users = JSON.parse(file);
const { successResponse } = require("../helpers/response");

class Users {
  constructor() {
    this.users = users;
  }

  // Menulis di file users.json
  writeToFile = () => {
    fs.writeFileSync("./data/users.json", JSON.stringify(this.users));
  };
  // CREATE user
  insertUser = (req, res) => {
    const body = req.body;

    const param = {
      nama_lengkap: body.nama_lengkap,
      username: body.username,
      password: body.password,
      email: body.email,
    };

    this.users.push(param);
    successResponse(res, 201, param);
    this.writeToFile();
  };

  // READ user
  getUsers = (req, res) => {
    successResponse(res, 200, this.users, {
      total: this.users.length,
    });
  };

  // UPDATE user
  updateUser = (req, res) => {
    const index = req.params.index;
    const body = req.body;

    this.users[index].nama_lengkap = body.nama_lengkap;
    this.users[index].username = body.username;
    this.users[index].password = body.password;
    this.users[index].email = body.email;
    successResponse(res, 200, this.users[index]);
    this.writeToFile();
  };

  // DELETE user
  deleteUser = (req, res) => {
    const index = req.params.index;

    this.users.splice(index, 1);
    successResponse(res, 200, null);
    this.writeToFile();
  };
}

module.exports = Users;
