const messages = {
  200: "sukses",
  201: "data berhasil simpan",
};
function successResponse(res, code, data, meta = {}) {
  res.status(200).json({
    data: data,
    meta: {
      code: code,
      message: messages[code.toString()],
      ...meta,
    },
  });
}

module.exports = { successResponse };
