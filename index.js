const express = require("express");
const path = require("path");
const api = require("./routes/api.js");
const morgan = require("morgan");

const app = express();
const port = 3000;

// app.use(logger);

// Set View Engine
app.set("view engine", "ejs");

// load static file
app.use(express.static(__dirname + "/public"));

// third party Middleware for logging
app.use(morgan("dev"));
app.get("/", function (req, res) {
  const name = req.query.name || "Starbucks";

  res.render(path.join(__dirname, "./views/index.ejs"), { name });
});
app.get("/menu", function (req, res) {
  res.render(path.join(__dirname, "./views/menu.ejs"));
});
app.get("/test", function (req, res) {
  res.json({
    test: "world",
  });
});

// load api routes
app.use("/api", api);

// internal server error handle middleware
app.use(function (err, req, res, next) {
  console.log(err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

// 404 handler middleware
app.use(function (req, res, next) {
  res.status(404).json({
    status: "fail",
    errors: "are you lost?",
  });
});

app.listen(port, () => {
  console.log("Server berhasil dijalankan");
});
