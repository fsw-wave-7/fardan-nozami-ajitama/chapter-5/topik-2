const express = require("express");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const Post = require("../controllers/post.js");
const post = new Post();

const Users = require("../controllers/users.js");
const users = new Users();

const logger = (req, res, next) => {
  console.log(`LOG : ${req.method} ${req.url}`);
  next();
};

const api = express.Router();
api.use(logger);
api.use(jsonParser);

// post
api.get("/post", post.getPost);
api.get("/post/:index", post.getDetailPost);
api.post("/post", post.insertPost);
api.put("/post/:index", post.updatePost);
api.delete("/post/:index", post.deletePost);

// user
api.post("/user", users.insertUser);
api.get("/users", users.getUsers);
api.put("/user/:index", users.updateUser);
api.delete("/user/:index", users.deleteUser);

module.exports = api;
